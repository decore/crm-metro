$(document).ready(function(){

	// required bolder
	$('[required]').parent().prev().css('font-weight', 'bolder');

	// ickech
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue'
	});

	// submenu
	$('.parameters li a').click(function(){
		$(this).parent().parent().addClass('active').siblings().removeClass('active');
	});
});